import { Game } from './backend/Game';
import { Player } from './backend/Player';
import { Kill } from './backend/Kill';
import { Chat } from './backend/Chat';

export interface GameSession extends Game {
  CurrentPlayer?: Player;
  Kills?: Kill[];
  messages?: Chat[];
}
