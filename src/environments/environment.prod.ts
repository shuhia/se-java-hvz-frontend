export const environment = {
  production: true,
  BACKEND_API_URL: 'https://se-java-hvz-backend.herokuapp.com/api/v1',
  MAPTILER_KEY: 'r6Xmy9XDmQurjMG2iIlS',
  BACKEND_SOCKET_URL: 'https://se-java-hvz-backend.herokuapp.com/ws',
  AUTHENTICATION_URL: 'https://shuhia-keycloak-auth-server.herokuapp.com/auth',
};
