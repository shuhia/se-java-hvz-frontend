import {Injectable} from '@angular/core';
import {KeycloakService} from "keycloak-angular";
import jwtDecode from "jwt-decode";

interface keycloakJwt {
  preferred_username: String;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  jwt: any | keycloakJwt;

  constructor(private keycloakService: KeycloakService) {
    keycloakService.getToken().then((token) => {
      this.jwt = jwtDecode(token);
    }).catch(error => console.log("failed to decode jwt token"));
  }

  public getUsername(): Promise<String> {
    return this.keycloakService.getToken().then((token) => {
      this.jwt = jwtDecode(token);
      return this.jwt.preferred_username;
    }).catch(error => {
      console.log("failed to get username from token")
    });
  }

  public getRoles(): Promise<String[]> {
    return this.keycloakService.getToken().then((token) => {
      this.jwt = jwtDecode(token);
      return this.jwt.roles;
    }).catch(error => {
      console.log("failed to get roles from token")
      return [];
    });
  }

  public isLoggedIn(): Promise<boolean> {
    return this.keycloakService.isLoggedIn().catch(error => {
      console.log("failed to check if user was logged in")
      return false;
    });
  }

  public register() {
    return this.keycloakService.register().catch(error => {
      console.log("failed to register")
      return false;
    });
  }

  public login() {
    return this.keycloakService.login().catch(error => {
      console.log("failed to login")
      return false;
    });
  }

  public logout() {
    return this.keycloakService.logout(window.location.origin).catch(error => {
      console.log("failed to logout")
      return false;
    });
  }

  public isAdmin() {
    return this.getRoles().then(roles => roles.findIndex(role => role === "admin") > -1).catch(error => {
      console.log("failed to get roles")
      return false;
    });
  }
}
