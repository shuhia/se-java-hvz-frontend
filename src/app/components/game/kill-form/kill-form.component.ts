import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import jwtDecode from 'jwt-decode';
import { KeycloakService } from 'keycloak-angular';
import { PlayerService } from 'src/app/services/backend/player.service';
import { KillService } from '../../../services/backend/kill.service';
import { Kill } from '../../../models/backend/Kill';

@Component({
  selector: 'app-kill-form',
  templateUrl: './kill-form.component.html',
  styleUrls: ['./kill-form.component.css'],
})
export class KillFormComponent implements OnInit {
  @Output() onKill = new EventEmitter<Kill>();
  player: any = 0;
  killerPlayer: string = '';
  @Input() killLatitude: number = 0;
  @Input() killLongitude: number = 0;
  gameId: number = Number(this.route.snapshot.paramMap.get('id'));

  constructor(
    private killService: KillService,
    private keycloakService: KeycloakService,
    private route: ActivatedRoute,
    private playerService: PlayerService
  ) {}

  ngOnInit(): void {
    this.keycloakService.getToken().then((token) => {
      const jwt: any = jwtDecode(token);
      this.killerPlayer = jwt.preferred_username;
    });
  }

  // Construct the kill object
  registerKill(formData: NgForm) {
    // Get data from input form
    const biteCode = formData.value.victimBiteCode;

    this.playerService
      .getPlayerIdByGameIdAndPlayerName(this.gameId, this.killerPlayer)
      .subscribe({
        next: (value) => {
          this.player = value.payload;
          this.killService
            .registerkillOnPlayer(this.gameId, {
              biteCode: biteCode,
              killerId: this.player.playerId,
              latitude: this.killLatitude,
              longitude: this.killLongitude,
              gameId: this.gameId,
            })
            .subscribe({
              next: (response) => {
                if (response.success) {
                  const kill = response.payload;
                  this.onKill.emit(kill);
                  console.log('Added Kill');
                } else {
                  alert(response.error.message);
                }
              },

              error(msg) {
                console.log(msg);
              },
            });
        },
      });
  }
}
