import {Component, OnInit} from '@angular/core';
import {KeycloakService} from 'keycloak-angular';
import {Game} from 'src/app/models/backend/Game';
import {GameService} from 'src/app/services/backend/game.service';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})
export class GameListComponent implements OnInit {

  // Get all games
  games: Game[] = [];
  isLoggedIn = false;

  constructor(
    private gameService: GameService,
    private keyCloakService: KeycloakService,
  ) {
  }

  ngOnInit(): void {
    this.keyCloakService.isLoggedIn().then(result => this.isLoggedIn = result);
    this.gameService.getGames().subscribe({
      next: value => {
        this.games = value.payload;
      }
    });
  }

}
