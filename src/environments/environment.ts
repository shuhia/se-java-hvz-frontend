// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  BACKEND_API_URL: 'https://se-java-hvz-backend.herokuapp.com/api/v1',
  MAPTILER_KEY: 'r6Xmy9XDmQurjMG2iIlS',
  BACKEND_SOCKET_URL: 'https://se-java-hvz-backend.herokuapp.com/ws',
  AUTHENTICATION_URL: 'https://shuhia-keycloak-auth-server.herokuapp.com/auth',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
