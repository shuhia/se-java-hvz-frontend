import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Chat } from '../models/backend/Chat';
import { Client, Subscription } from 'stompjs';
import { AuthService } from './auth/auth.service';
import { ClientService } from './client.service';

@Injectable({
  providedIn: 'root',
})
export class ChatService {
  public message$: Subject<Chat> = new Subject();
  basePath = '';
  destination = '';
  client: Client | any;
  channel: String = 'global';
  private faction: String = '';
  private subscriptions: Subscription[] = [];

  constructor(
    private authService: AuthService,
    private clientService: ClientService
  ) {}

  /**
   * Connect to chat
   * @param gameId - id of game
   * @returns - a client object
   */
  public async connectTo(gameId: number = 0): Promise<Client | undefined> {
    this.basePath = '/topic/game/' + gameId;
    this.client = await this.clientService.client;
    return this.client;
  }

  public disconnect() {
    if (this.client) this.client.disconnect(() => {});
  }

  /**
   * Switch channel. Unsubscribes to all other channels. Subscribes to target channel.
   * @param channel
   */
  public async switchChannel(channel: String, faction: String = '') {
    // unsubscribe from all channels
    if (this.client) {
      this.unsubscribeAll();
      this.subscribeTo(channel, faction);
    }
  }

  public subscribeTo(channel: String, faction: String) {
    const client: Client = this.client;
    this.channel = channel;
    this.faction = faction;
    this.destination = this.basePath + '/' + this.channel;
    if (faction) {
      this.destination += '/' + faction;
    }
    const subscription = client.subscribe(this.destination, (message) => {
      console.log(message);
      const chatMessage = JSON.parse(message.body);
      this.message$.next(chatMessage);
    });
    this.subscriptions.push(subscription);
  }

  /**
   * Sends message to the specified channel
   * @param message - message
   * @param channel - default channel = global
   */
  public sendMessage(message: Chat) {
    const connection = this.client;
    // Sends message to global
    connection.send(this.destination, () => {}, JSON.stringify(message));
  }

  /**
   * Gets the message container
   */

  public getNewMessage = () => {
    return this.message$.asObservable();
  };

  unsubscribeAll() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
    this.subscriptions = [];
  }
}
