import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlayerService } from 'src/app/services/backend/player.service';
import { Player } from '../../../models/backend/Player';

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.css'],
})
export class PlayerListComponent implements OnInit {
  @Input() playersInGame: Player[] = [];

  @Input() isAdmin = false;
  @Output() flipState = new EventEmitter<any>();

  constructor(
    private playerService: PlayerService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {}

  public onFlipState(playerId: number) {
    this.flipState.emit(playerId);
  }

  public handleKickPlayer(playerId: number) {
    const gameId = Number(this.route.snapshot.paramMap.get('id'));
    if (
      window.confirm(
        'Are you sure that you want to kick player with id: ' + playerId
      )
    ) {
      this.playerService
        .deletePlayerByGameIdAndPlayerId(gameId, playerId)
        .subscribe({
          next: (response) => {
            if (response.success) {
              alert(`Removed player`);
              // Remove player from player list

              this.playersInGame = this.playersInGame.filter(
                (player) => player.playerName !== response.payload.playerName
              );
            } else {
              alert(response.error.message);
            }
          },
        });
    }
  }
}
