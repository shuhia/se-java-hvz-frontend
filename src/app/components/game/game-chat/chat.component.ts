import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ChatService } from '../../../services/chat.service';
import { Chat } from '../../../models/backend/Chat';
import { PlayerService } from '../../../services/backend/player.service';
import { Player } from '../../../models/backend/Player';
import { GameService } from '../../../services/backend/game.service';
import { AuthService } from '../../../services/auth/auth.service';
import { NgForm } from '@angular/forms';
import { gameStates } from '../../../models/backend/Game';

class ChatMessage implements Chat {
  gameId: Number = 1;
  message: String;
  playerName: String;
  faction: any;
  channel: String;

  constructor(
    message: String,
    playerName: String = 'playerName',
    gameId: Number,
    faction: String,
    channel: String
  ) {
    this.message = message;
    this.playerName = playerName;
    this.gameId = gameId;
    this.faction = faction;
    this.channel = channel;
  }
}

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
})

/***
 * Process
 * 1. Connect to server with websocket
 * 2. Set destination
 * 3. Set channel
 */
export class ChatComponent implements OnInit, OnDestroy {
  @Input() title = 'title';
  @Input() messageList: Chat[] = [];
  @Input() playerList: Player[] = [];
  @Input() gameId = 1;
  @Input() player: any | Player = null;
  @Input() gameState = gameStates.register;
  username: String = '';
  message = '';
  players: Player[] = [];
  isConnected: boolean = false;
  isAdmin: boolean = false;
  channel: String = 'global';
  faction: String | null = null;
  channelFaction: String = '';

  constructor(
    private chatService: ChatService,
    private playerService: PlayerService,
    private gameService: GameService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.chatService.unsubscribeAll();
  }

  async init() {
    try {
      this.isAdmin = await this.authService.isAdmin();
      this.username = await this.authService.getUsername();
      this.getPlayer();
      this.getPlayerList();
      this.subscribeToMessages();
      // Connect to websocket

      await this.connect();
      // Subscribe to global by default

      this.switchChannel('global');
      // get messageList from service
      this.getMessageList();
    } catch (e) {
      console.log(e);
      console.log('Failed to init chat component');
    }
  }

  /***
   * User can only send a message if they registered to the game
   * @param message
   */
  // Sends message to server
  public sendMessage(message = this.message) {
    if (this.player || this.isAdmin) {
      // Saves message by rest endpoint
      console.log(this.gameId);
      // Create message
      const chatMessage: Chat = new ChatMessage(
        message,
        this.username,
        this.gameId,
        this.getFaction(),
        this.getChannel()
      );
      this.gameService.postChatMessage(this.gameId, chatMessage).subscribe({
        next: (response) => console.log(response.payload),
        error: (msg) => console.log(msg),
      });
      // Sends message to websocket
      this.chatService.sendMessage(chatMessage);
      // Reset message
      this.message = '';
    } else {
      alert(
        'You have to be registered to this game or an admin in order to send a message'
      );
    }
  }

  handleSendMessage(messageForm: NgForm) {
    if (messageForm.valid) {
      const { message } = messageForm.value;
      this.sendMessage(message);
    }
  }

  public switchChannel(channel: String, faction: String = '') {
    this.channel = channel;

    this.chatService.switchChannel(channel, faction);
  }

  public joinGlobal() {
    this.switchChannel('global');
  }

  joinFaction(faction: String = this.getFaction()) {
    this.channelFaction = faction;
    this.switchChannel('faction', faction);
  }

  public getFaction() {
    if (this.isAdmin) {
      return this.channelFaction;
    }
    if (this?.player?.human === true) {
      return 'humans';
    } else if (this?.player?.human === false) {
      return 'zombies';
    } else {
      return '';
    }
  }

  filterMessage() {
    // Filter by channel
    if (this.channel === 'global') {
      return this.messageList.filter(
        (message) => message.channel === this.channel
      );
    } else
      return this.messageList
        .filter((message) => message.channel === this.channel)
        .filter((message) => {
          if (this.channelFaction) {
            return message.faction === this.channelFaction;
          } else return this.isAdmin;
        });
  }

  public showFactionButton() {
    return this.gameState === gameStates.started;
  }

  private async connect() {
    this.isConnected = false;
    await this.chatService.connectTo(this.gameId);
    this.isConnected = true;
  }

  private getMessageList() {
    this.chatService.getNewMessage().subscribe({
      next: (message: Chat) => {
        if (!message.playerName) {
          if (message.playerId) {
            // get playerName from player list
            const player = this.findPlayerById(message.playerId);
            const playerName = player?.playerName;
            if (playerName) {
              message.playerName = playerName;
            }
          }
        }
        this.messageList.push(message);
      },
    });
  }

  private subscribeToMessages() {
    this.gameService.getChatByGameId(this.gameId).subscribe({
      next: (response) => {
        this.messageList = response.payload;
      },
      error: (message) => {
        console.log('failed to get message list from backend');
      },
    });
  }

  private getPlayerList() {
    this.playerService.getPlayersByGameId(this.gameId).subscribe({
      next: (value) => {
        this.players = value.payload;
        this.playerList = value.payload;
      },
    });
  }

  private getPlayer() {
    this.playerService
      .getPlayerIdByGameIdAndPlayerName(this.gameId, this.username)
      .subscribe(
        (next) => {
          this.player = next.payload;
        },
        (error) => {
          alert('failed to get player' + error);
        }
      );
  }

  private findPlayerById(id: String | number) {
    return this.playerList.find((player) => player?.playerId === id);
  }

  private getChannel() {
    return this.channel;
  }
}
