# Human vs Zombies Frontend

## Description

This project is about creating a frontend application for the tag-like game Human vs Zombies. It is one of the final
projects for the fullstack course on noroff.

## Preview

https://se-java-hvz-frontend.netlify.app/

#### Landing page

![src/assets/Humans Vs Zombie Landing Page.png](src/assets/Humans Vs Zombie Landing Page.png "Landing page")

## Getting Started

### Dependencies

- AngularJS
- Leaflet
- Faker
- NodeJs

### Preparation

Setup the backend by cloning and deploying this project:

- https://gitlab.com/shuhia/se-java-hvz-1

Clone the project

``
git clone https://gitlab.com/shuhia/se-java-hvz-1.git
``

In the project folder
``
src/environments
``
adjust the variables values if you are using your own server.

````javascript 
{
// Change to your backend 
BACKEND_API_URL: 'https://se-java-hvz-backend.herokuapp.com/api/v1',
// Change to your MAPTILER_KEY
MAPTILER_KEY: 'H9oJW9rxczRBp6VL8obc',
// Change to your backend socket url 
BACKEND_SOCKET_URL: 'https://se-java-hvz-backend.herokuapp.com/ws',
// change to your Authentication url 
AUTHENTICATION_URL: 'https://shuhia-keycloak-auth-server.herokuapp.com/auth',
}
````

### Installing

Open the project on an editor like vscode or IntelliJ. To install all dependencies Run

``npm install``

### Executing program

Starting the server on http://localhost:4200/

```
ng serve 
```

### Using the app

Register as a user and join a game
https://www.awesomescreenshot.com/video/8140254?key=e3ac91575577076b1496421724c6399b

Login as admin and create a game
https://www.awesomescreenshot.com/video/8140379?key=a80d4357b3ecdced6f1b735a9fcf3fa2

## Authors

- Alex On
- Christopher Westman
- Nils Jacobsen
- Sebastian Börjesson
